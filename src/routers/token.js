const express = require('express')
const TokenRequest = require('../requests/TokenRequest')
const TokenController = require('../controllers/TokenController')

const router = express.Router()

router.post('/tokens', TokenRequest, TokenController.tokens)

module.exports = router
