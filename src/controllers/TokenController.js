const BinlistProvider = require('../providers/Binlist')
const TokenResponse = require('../responses/TokenResponse')

exports.tokens = async (req, res, next) => {
  try {
    const { data } = req

    const result = await BinlistProvider.getBrand(data)
    const brand = result.data.scheme

    const response = TokenResponse.token({ ...data, brand })

    res.status(200).send({
      ...response
    })
  } catch (error) {
    res.status(500).send({ error: error.message })
  }
}
