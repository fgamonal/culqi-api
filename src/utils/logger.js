const fs = require('fs')
const appRoot = require('app-root-path')
const { createLogger, format, transports } = require('winston')

const logDir = `${appRoot}/logs`
if (!fs.existsSync(logDir)) {
  fs.mkdirSync(logDir)
}
const filename = `${logDir}/app.log`

// { error: 0, warn: 1, info: 2, verbose: 3, debug: 4, silly: 5 }
const env = process.env.NODE_ENV || 'development'
const logger = createLogger({
  level: env === 'development' ? 'debug' : 'info',
  format: format.combine(
    format.timestamp({
      format: 'YYYY-MM-DD HH:mm:ss'
    }),
    format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
  ),
  transports: [
    new transports.Console({
      level: 'info',
      format: format.combine(
        format.colorize(),
        format.printf(
          info => `${info.timestamp} ${info.level}: ${info.message}`
        )
      )
    }),
    new transports.File({ filename })
  ]
})

logger.stream = {
  write: function(message, encoding) {
    logger.info(message)
  }
}

module.exports = logger
