const getDigits = (card) => {
  const length = 6
  return card.substring(0, length)
}

module.exports = {
  getDigits
}
