const DateUtils = require('../utils/date')
const logger = require('../utils/logger')

const token = (data) => {
  // {"token": "tkn_live_4444333322221111-2020-10", "brand": "visa", "creation_dt": "2019-01-01 18:00:00"}
  const token = generateToken(data)
  const date = DateUtils.format(new Date())

  const response = { token, brand: data.brand, creation_dt: date }

  logger.info(JSON.stringify(response, null, 2))

  return response
}

const generateToken = (data) => {
  // "tkn_live_" + <pan> "-" + <exp_year> + "-" + <exp_month>
  const prefix = 'tkn_live_'
  const { pan, exp_year, exp_month } = data
  const separator = '-'
  const expiration_month = String(exp_month).padStart(2, '0')

  return `${prefix}${pan}${separator}${exp_year}${separator}${expiration_month}`
}

module.exports = {
  token
}
