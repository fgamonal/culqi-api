const axios = require('axios')
const logger = require('../utils/logger')
const card = require('../utils/card')

const URL_API = 'https://lookup.binlist.net'

const getBrand = async (data) => {
  try {
    const { pan } = data
    const param = card.getDigits(pan)
    const result = await axios.get(`${URL_API}/${param}`)

    logger.info(JSON.stringify(result.data, null, 2))
    return result

  } catch (error) {
    let message = ''
    if (error.response) {
      if (error.response.status === 400) {
        message = 'Bad Request'
      } else {
        message = error.response.data
      }
    } else if (error.request) {
      message = 'Bad Request'
    } else {
      message = error.message
    }
    logger.error(message)
    throw new Error(message)
  }
}

module.exports = {
  getBrand
}
