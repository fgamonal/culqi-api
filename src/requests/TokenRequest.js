const Joi = require('@hapi/joi');
const logger = require('../utils/logger')

const tokens = async (req, res, next) => {
  try {
    const data = req.body
    logger.info(JSON.stringify(data, null, 2))

    const schema = Joi.object().keys({
      pan: Joi.string().regex(/^[0-9]*$/).min(16).max(16).required(),
      exp_year: Joi.number().integer().min(2019).max(2026),
      exp_month: Joi.number().integer().min(1).max(12)
    })

    const result = Joi.validate(data, schema)
    if (result.error) {
      const errors = result.error.details

      const detailErrors = JSON.stringify(errors, null, 2)
      logger.error(detailErrors)

      const message = errors.map(error => error.message)
      const messageString = message.toString()
      throw new Error(messageString)
    }

    req.data = data

    next()
  } catch (error) {
    res.status(400).send({ message: error.message })
  }
}

module.exports = tokens
