const express = require('express')
const morgan = require('morgan')
const logger = require('./utils/logger')

const app = express()

app.use(express.json())
app.use(morgan('combined', { stream: logger.stream }))

const culqiRouter = require('./routers/token')
app.use(culqiRouter)

module.exports = app
