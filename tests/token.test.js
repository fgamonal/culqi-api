const request = require('supertest')
const app = require('../src/app')

const data = require('../src/data/token')

/* beforeEach(() => {
  console.log('beforeEach')
})

afterEach(() => {
  console.log('afterEach')
}) */

test('Debería obtener el resultado correcto', async() => {
  const response = await request(app).post('/tokens').send(data.request).expect(200)

  expect(Object.keys(response.body).sort()).toEqual(['brand', 'creation_dt', 'token'])
})

test('Debería obtener un error 400, ya que no envío los parametros correctos', async() => {
  const response = await request(app).post('/tokens').send(data.requestError).expect(400)
})
